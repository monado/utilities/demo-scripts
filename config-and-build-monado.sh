#!/bin/bash
TOP_DIR="$PWD"
INSTALL_DIR="$TOP_DIR/install"
BUILD_DIR="$TOP_DIR/build-monado"

export PKG_CONFIG_PATH="$INSTALL_DIR/lib/pkgconfig"

mkdir -p "$BUILD_DIR"
pushd "$BUILD_DIR"
cmake ../Monado -G Ninja \
	-DCMAKE_INSTALL_PREFIX=$INSTALL_DIR
popd

ninja -C "$BUILD_DIR"
