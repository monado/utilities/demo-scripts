#!/bin/bash
TOP_DIR=$PWD
INSTALL_DIR="$TOP_DIR/install"

export LD_LIBRARY_PATH="$INSTALL_DIR/lib"
export XR_RUNTIME_JSON="$TOP_DIR/build-monado/openxr_monado-dev.json"

install/bin/hello_xr --graphics Vulkan $@
