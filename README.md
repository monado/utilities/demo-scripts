# Demo Scripts

Small helper scripts to get Monado running on your system. Only tested on
Ubuntu 19.10.


## Requirments

* Ubuntu 19.10 (others may work, Ubuntu 18.04 and earlier is known not to
  work).
* AMD or Intel graphics hardware. (On Intel graphics, only Vulkan demos work
  due to a missing OpenGL extension in the Intel driver.)


## Basic dependencies

First get the build dependancies, this should be all of them but some might be
missing.

```bash
sudo apt install git cmake meson ninja-build doxygen graphviz libeigen3-dev \
	python3 pkg-config libx11-dev libxxf86vm-dev libxrandr-dev \
	libxcb-randr0-dev libhidapi-dev libwayland-dev libvulkan-dev \
	glslang-dev glslang-tools libglvnd-dev libgl1-mesa-dev wget libglm-dev \
	libusb-1.0-0-dev libuvc-dev libavcodec-dev libopencv-dev libudev-dev \
	libv4l-dev libegl1-mesa-dev libglfw3-dev libsdl2-dev
```

In order for Monado to get access to some of the hardware we need to install
some udev rules, we provide a package for this that you can install like this:

```bash
wget https://gitlab.freedesktop.org/monado/utilities/xr-hardware/uploads/b83650f05d6c8fa4966b4f4d0d489cd3/xr-hardware_0.2.1-1_all.deb
sudo dpkg -i xr-hardware_0.2.1-1_all.deb
```

The last thing we need to grab is the source, from this directory run the below
command to clone all of the needed repositories.

```bash
./clone.sh
```

## Building

Use the provided scripts to build all of the needed components. The OpenXR step
will "install" the loader into the `install` subdirectory, rather than
system-wide.

```bash
./config-and-build-openxr.sh
./config-and-build-monado.sh
./config-and-build-seethrough.sh
```

## Running

### Hello XR

To run the HelloXR sample application use the provided script. The script
currently selects Vulkan for rendering by default, but can be changed with
the argument `--graphics OpenGL`. See `-h` for help. Press `enter` in the CLI
window to stop the demo.

```bash
# Defaults no arguments, uses Vulkan.
./run-hello_xr.sh
# Select OpenGL for rendering, does not work on Intel GPUs.
./run-hello_xr.sh --graphics OpenGL
```


### Video see-through demo

To run the video/camera see-through demo (original HTC Vive only), run the
following command. Press `ESC` in the mirror window to stop the demo.

```bash
# Defaults
./run-seethrough.sh
# Camera and format selected, both are optional.
./run-seethrough.sh --device /dev/video2 --format NV12
```

**Notes:**

* This is a fairly early sample and has not been a target for optimization. It's
  more of a proof of concept, and may be uncomfortable to use.
* If you have other cameras connected to the computer it might pick the wrong
  one. Then you must give it the `--device /dev/videoX` where `X` is the number
  for the HMD camera.
* Since this demo uses OpenGL it will not work on Intel GPUs.
